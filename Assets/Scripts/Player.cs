﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using System;

public class Player : Character
{

    private static Player instance;

    [SerializeField]
    private Transform[] groundPoints;
    [SerializeField]
    private float groundRadius;
    [SerializeField]
    private LayerMask whatIsGround;

    [SerializeField]
    private bool airControl;

    [SerializeField]
    private float jumpForce;
    public Rigidbody2D MyRigidBody { get; set; }
    public int MyProperty { get; set; }

    public bool FlashStep { get; set; }

    public bool Jump { get; set; }

    public bool OnGround { get; set; }

    public override bool IsDead
    {
        get
        {
            return Health <= 0;
        }
    }

    //private Vector2 startPos;

    public static Player Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<Player>();
            }
            return instance;
        }

    }

    private Vector2 startPos;

    // Use this for initialization
    public override void Start()
    {

        base.Start();
        // startPos = transform.position;
        MyRigidBody = GetComponent<Rigidbody2D>();


    }

    void Update()
    {
        if (!TakingDamage)
        {
            if (transform.position.y <= -14f)
            {
                MyRigidBody.velocity = Vector2.zero;
                transform.position = startPos;
            }
            HandleInput();
        }

    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (!TakingDamage && !IsDead)
        {
            float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
            OnGround = IsGrounded();
            HandleMovement(horizontal);
            flip(horizontal);
            HandleLayers();
        }
    }

    private void HandleMovement(float horizontal)
    {
        if (MyRigidBody.velocity.y < 0)
        {
            MyAnimator.SetBool("land", true);
        }
        if (!Attack && !FlashStep || (OnGround || airControl))
        {
            MyRigidBody.velocity = new Vector2(horizontal * movementSpeed, MyRigidBody.velocity.y);
        }
        if (Jump && MyRigidBody.velocity.y == 0)
        {
            MyRigidBody.AddForce(new Vector2(0, jumpForce));
        }

        MyAnimator.SetFloat("speed", Mathf.Abs(horizontal));
    }


    private void HandleInput()
    {
        if (CrossPlatformInputManager.GetAxis("Vertical") > 0)
        {
            MyAnimator.SetTrigger("jump");
        }
        if (CrossPlatformInputManager.GetButton("Jump"))
        {
            MyAnimator.SetTrigger("attack");
        }
        if (CrossPlatformInputManager.GetButton("combo"))
        {
            MyAnimator.SetTrigger("getsuga");
        }
    }


    private void flip(float horizontal)
    {
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
            ChangeDirection();
        }
    }



    private bool IsGrounded()
    {
        if (MyRigidBody.velocity.y <= 0)
        {
            foreach (Transform point in groundPoints)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);
                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject != gameObject)
                    {

                        return true;
                    }
                }
            }

        }
        return false;
    }

    private void HandleLayers()
    {
        if (!OnGround)
        {
            MyAnimator.SetLayerWeight(1, 1);
        }
        else
        {
            MyAnimator.SetLayerWeight(1, 0);
        }
    }

    public override void ThrowGetsugaTenshou(int value)
    {
        if (!OnGround && value == 1 || OnGround && value == 0)
        {
            base.ThrowGetsugaTenshou(value);
        }
    }

    public override IEnumerator TakeDamage()
    {

        Health -= 10;
        if (!IsDead)
        {
            MyAnimator.SetTrigger("damage");
        }
        else
        {
            MyAnimator.SetLayerWeight(1, 0);
            MyAnimator.SetTrigger("death");
        }
        yield return null;
    }
}
