﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class GetsugaTenshou : MonoBehaviour
{

    [SerializeField]
    private float speed;

    private Rigidbody2D myRigidBody;

    private Vector2 direction;
    // Use this for initialization
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();

    }

    void FixedUpdate()
    {
        myRigidBody.velocity = direction * speed;
    }

    // Update is called once per frame

    public void Initialize(Vector2 direction)
    {
        this.direction = direction;
    }


    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }


}
