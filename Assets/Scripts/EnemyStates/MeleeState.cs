﻿using UnityEngine;
using System.Collections;
using System;

public class MeleeState : IEnemyState
{


    private Enemy enemy;
    private float attackTimer;
    private float attackCoolDown = 1;
    private bool canAttack = true;
    public void Enter(Enemy enemy)
    {
        this.enemy = enemy;
    }

    public void Execute()
    {
        attackSlash();
        if(enemy.InThrowRange && !enemy.InMeleeRange)
        {
            enemy.ChangeState(new RangedState());
        }else if(enemy.Target == null)
        {
            enemy.ChangeState(new IdleState());
        }
    }

    public void Exit()
    {
       
    }

    public void OnTriggerEnter(Collider2D other)
    {
        
    }
    private void attackSlash()
    {
        attackTimer += Time.deltaTime;

        if (attackTimer >= attackCoolDown)
        {
            canAttack = true;
            attackTimer = 0;
        }
        if (canAttack)
        {
            enemy.MyAnimator.SetTrigger("attack");
        }
    }
}
