﻿using UnityEngine;
using System.Collections;
using System;

public class PatrolState : IEnemyState
{

    private Enemy enemy;
    private float patrolTimer;
    private float patrolDuration = 10;
  
    public void Enter(Enemy enemy)
    {
        this.enemy = enemy;
    }

    public void Execute()
    {
        Debug.Log("Patrolling");
        Patrol();
        enemy.Move();

        if (enemy.Target != null && enemy.InThrowRange)
        {
            enemy.ChangeState(new RangedState());
        }
        
    }

    public void Exit()
    {
        
    }

    public void OnTriggerEnter(Collider2D other)
    { 
        if (other.tag == "Edge")
        {
            enemy.ChangeDirectionEnemy();
        }
    }

    private void Patrol()
    {
        patrolTimer += Time.deltaTime;

        if (patrolTimer >= patrolDuration)
        {
            enemy.ChangeState(new IdleState());
        }
    }
}
