﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy : Character
{

    [SerializeField]
    private float MeleeRange;
    [SerializeField]
    private float ThrowRange;
    public bool InMeleeRange
    {
        get
        {
            if (Target != null)
            {
                return Vector2.Distance(transform.position, Target.transform.position) <= MeleeRange;
            }
            return false;
        }
    }
    public bool InThrowRange
    {
        get
        {
            if (Target != null)
            {
                return Vector2.Distance(transform.position, Target.transform.position) <= ThrowRange;
            }
            return false;
        }
    }
    private IEnemyState currentState;

    public GameObject Target { get; set; }

    public override bool IsDead
    {
        get
        {
            return Health <= 0;
        }
    }

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        ChangeState(new IdleState());
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsDead)
        {

            if (!TakingDamage)
            {
                currentState.Execute();
            }

            LookAtTarget();
        }
    }

    private void LookAtTarget()
    {
        if (Target != null)
        {
            float xDir = Target.transform.position.x - transform.position.x;

            if (xDir < 0 && facingRight || xDir > 0 && !facingRight)
            {
                ChangeDirectionEnemy();
            }
        }
    }

    public void ChangeState(IEnemyState newState)
    {
        if (currentState != null)
        {
            newState.Exit();
        }

        currentState = newState;
        currentState.Enter(this);

    }

    public void Move()
    {

        if (!Attack)
        {
            MyAnimator.SetFloat("speed", 1);
            transform.Translate(GetDirection() * (movementSpeed * Time.deltaTime));
        }

    }

    public void ChangeDirectionEnemy()
    {
        if (facingRight)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(transform.localScale.x, 1, 1);
        }
    }

    public Vector2 GetDirection()
    {
        return facingRight ? Vector2.right : Vector2.left;
    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
        currentState.OnTriggerEnter(other);
    }
    public override void ThrowGetsugaTenshou(int value)
    {
        base.ThrowGetsugaTenshou(value);
    }
    public override IEnumerator TakeDamage()
    {
        Health -= 10;

        if (!IsDead)
        {
            MyAnimator.SetTrigger("damage");
        }
        else
        {
            MyAnimator.SetTrigger("death");
            yield return null;
        }
    }
}
