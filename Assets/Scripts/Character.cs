﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Character : MonoBehaviour
{
    [SerializeField]
    protected int Health;

    [SerializeField]
    private EdgeCollider2D swordCollider;
    [SerializeField]
    private List<string> damageSources;

    public abstract bool IsDead { get; }
    public Animator MyAnimator { get; private set; }

    protected bool facingRight;

    public bool Attack { get; set; }

    public bool TakingDamage { get; set; }

    public EdgeCollider2D SwordCollider
    {
        get
        {
            return swordCollider;
        }
    }

    [SerializeField]
    protected Transform getsugaPositon;

    [SerializeField]
    protected float movementSpeed;

    [SerializeField]
    private GameObject getsugaPrefab;
    // Use this for initialization
    public virtual void Start()
    {

        facingRight = true;
        MyAnimator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeDirection()
    {
        facingRight = !facingRight;
        transform.localScale = new Vector3(transform.localScale.x * -1, 1, 1);
    }


    public virtual void ThrowGetsugaTenshou(int value)
    {
        if (facingRight)
        {
            GameObject tmp = (GameObject)Instantiate(getsugaPrefab, getsugaPositon.position, Quaternion.Euler(0, 0, 0));
            tmp.GetComponent<GetsugaTenshou>().Initialize(Vector2.right);
        }
        else
        {
            GameObject tmp = (GameObject)Instantiate(getsugaPrefab, getsugaPositon.position, Quaternion.Euler(0, 0, 180));
            tmp.GetComponent<GetsugaTenshou>().Initialize(Vector2.left);
        }
    }

    public abstract IEnumerator TakeDamage();
    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if(damageSources.Contains(other.tag))
        {
            StartCoroutine(TakeDamage());
        }
    }

    public void attackSword()
    {
        SwordCollider.enabled = true;
    }
}
